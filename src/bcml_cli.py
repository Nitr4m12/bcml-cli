#!/usr/bin/env python
from pathlib import Path
import argparse
import json

from bcml import install, dev
from bcml.util import BcmlMod, get_installed_mods, get_settings

def parse_args():
    parser = argparse.ArgumentParser(description="CLI commands for BCML")
    parser.add_argument("-d", "--disable", metavar="MODDIR", type=Path, help="Disable a BCML mod", nargs="?")
    parser.add_argument("-D", "--disable-all", action="store_true", help="Disable all installed mods")
    parser.add_argument("-e", "--enable", metavar="MODDIR", type=Path, help="Enable a BCML mod", nargs="?")
    parser.add_argument("-E", "--enable-all", action="store_true", help="Enable all installed mods")
    parser.add_argument("-b", "--bnpify", metavar="MODDIR", type=Path, help="Create a bnp mod", nargs="?")
    parser.add_argument("-o", "--export", metavar="OUTPUT", type=Path, help="Export the master modpack", nargs="?")
    parser.add_argument("-R", "--remerge", action="store_true", help="Perform a remerge")
    parser.add_argument("-i", "--install", metavar="MODDIR", type=Path, help="Install a mod", nargs="?")
    parser.add_argument("-p", "--priority", metavar="PRIORITY NUMBER", type=int, help="Set the priority when installing a mod", nargs="?")
    parser.add_argument("-u", "--uninstall", metavar="BCML_MODDIR", type=Path, help="Uninstall a mod from the bcml data directory", nargs="?")
    parser.add_argument("-U", "--uninstall-all", action="store_true", help="Uninstall a mod")
    parser.add_argument("-B", "--backup", metavar="BACKUP_NAME", help="Create a backup of the current mod list", nargs="?")
    parser.add_argument("-r", "--restore", metavar="BACKUP_PATH", type=Path, help="Restore a backup", nargs="?")
    
    return parser.parse_args()


def disable_all():
    for mod in get_installed_mods():
        install.disable_mod(mod=mod, wait_merge=True)
    install.refresh_merges()
    install.refresh_master_export()

def enable_all():
    for mod in get_installed_mods(disabled=True):
        if mod.disabled:
            install.enable_mod(mod=mod, wait_merge=True)
    install.refresh_merges()
    install.refresh_master_export()

def uninstall_all():
    for mod in get_installed_mods(disabled=True):
        install.uninstall(mod=mod, wait_merge=True)
    install.refresh_merges()
    install.refresh_master_export()

def create_bnp(args):
    info = args.bnpify / "info.json" if (args.bnpify / "info.json").exists() else args.bnpify / "rules.txt"
    if not info.exists():
        (args.bnpify / "info.json").write_text(
            json.dumps(
                {
                    "name": "Temp",
                    "desc": "Temp pack",
                    "url": "",
                    "id": "VGVtcD0wLjA=",
                    "image": "",
                    "version": "1.0.0",
                    "depends": [],
                    "options": {},
                    "platform": "wiiu"
                    if get_settings("wiiu")
                    else "switch",
                }
            )
        )
        info = args.bnpify / "info.json"

    if info.suffix == ".json":
        meta = json.loads(info.read_text("utf-8"))
        dev.create_bnp_mod(
            mod=args.bnpify,
            output=args.bnpify / f"{meta['name'].replace(' ', '')}.bnp",
            meta=meta,
        )

    else:
        # https://stackoverflow.com/questions/24264609/parsing-txt-file-into-dictionary-in-python
        meta = {}
        with info.open(encoding="utf-8") as rules:
            for line in rules:
                if "=" in line:
                    key, val = map(str.strip, line.split("="))
                    meta[key] = val
        dev.create_bnp_mod(
            mod=args.bnpify,
            output=args.bnpify / f"{meta['name'].strip()}.bnp",
            meta={
                "name": meta["name"],
                "version": meta["version"],
                "desc": meta["description"],
            },
        )
  
def main():
    args = parse_args()
    if args.disable:
        # --disable
        install.disable_mod(mod=BcmlMod(args.disable))

    elif args.disable_all:
        # --disable-all
        disable_all()

    elif args.enable:
        # --enable
        install.enable_mod(mod=BcmlMod(args.enable))

    elif args.enable_all:
        # --enable-all
        enable_all()

    elif args.remerge:
        # --remerge
        install.refresh_merges()
        install.refresh_master_export()

    elif args.install:
        # --install
        install.install_mod(mod=args.install, insert_priority=args.priority if args.priority else 0, merge_now=True)
        install.refresh_master_export()

    elif args.uninstall:
        # --uninstall
        install.uninstall_mod(mod=BcmlMod(args.uninstall))

    elif args.uninstall_all:
        # --uninstall
        uninstall_all()
    
    elif args.bnpify:
        # --bnpify
        create_bnp(args)

    elif args.export:
        # --export
        install.export(args.export)
    
    elif args.backup:
        # --backup
        install.create_backup(args.backup)

    elif args.restore:
        # --restore
        install.restore_backup(args.restore)